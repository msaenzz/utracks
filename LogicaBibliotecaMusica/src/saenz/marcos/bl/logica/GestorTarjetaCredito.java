package saenz.marcos.bl.logica;

import saenz.marcos.bl.entities.TarjetaCredito.TarjetaCredito;
import saenz.marcos.dl.DataTarjetaCredito;

public class GestorTarjetaCredito {
    private DataTarjetaCredito dataTarjetaCredito;

    public GestorTarjetaCredito(){
        dataTarjetaCredito = new DataTarjetaCredito();
    }

    public boolean buscarTarjetaCredito(int numeroTarjetaCredito){
        return dataTarjetaCredito.buscarTarjetaCredito(numeroTarjetaCredito);
    }

    public void registrarTarjetaCredito(int numeroTarjeta, int ccv, String nombreCompleto){
        TarjetaCredito tarjetaCredito = new TarjetaCredito( numeroTarjeta,  ccv,  nombreCompleto);
        dataTarjetaCredito.crearTarjetaCredito(tarjetaCredito);
    }

    public String listarTarjetasCredito(){
        return dataTarjetaCredito.imprimirTarjetaCredito();
    }
}
