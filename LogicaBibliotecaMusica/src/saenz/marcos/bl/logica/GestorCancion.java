package saenz.marcos.bl.logica;

import saenz.marcos.DAO.DAOFactory;
import saenz.marcos.bl.entities.Album.Album;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.bl.entities.Cancion.Cancion;
import saenz.marcos.bl.entities.Cancion.ICancionDAO;
import saenz.marcos.bl.entities.Compositor.Compositor;
import saenz.marcos.bl.entities.Genero.Genero;
import saenz.marcos.dl.DataCancion;
import saenz.marcos.utils.Utilities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class GestorCancion {
    private DataCancion dataCancion;

    public GestorCancion(){
        dataCancion = new DataCancion();
    }

    public boolean buscarCancion(String nombre){
        return dataCancion.buscarCancion(nombre);
    }

    public void registrarCancion(String url, String nombre, Genero genero, Artista artista, Compositor compositor, Album album, LocalDate fechaLanzamiento){
        try {
            Cancion cancion = new Cancion(genero,artista,compositor,album,fechaLanzamiento,nombre,url);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            ICancionDAO dao = factory.getCancionDAO();
            dao.registrarCancion(cancion);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Cancion> listarCanciones() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        ICancionDAO dao = factory.getCancionDAO();
        return dao.listarCanciones();
    }

    public ArrayList<Cancion> listarCancionesBiblio() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        ICancionDAO dao = factory.getCancionDAO();
        return dao.listarBiblioteca();
    }
}
