package saenz.marcos.bl.logica;

import saenz.marcos.DAO.DAOFactory;
import saenz.marcos.bl.entities.Compositor.Compositor;
import saenz.marcos.bl.entities.Compositor.ICompositorDAO;
import saenz.marcos.dl.DataCompositor;
import saenz.marcos.utils.Utilities;

import java.util.ArrayList;

public class GestorCompositor {

    private DataCompositor dataCompositor;

    public GestorCompositor(){
        dataCompositor = new DataCompositor();
    }

    public void registrarCompositor(String nombre, String apellido, String paiaNacimiento, int edad) throws Exception {
        try {
            Compositor compositor = new Compositor(nombre, apellido,paiaNacimiento,edad);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            ICompositorDAO dao = factory.getCompositorDAO();
            dao.registrarCompositor(compositor);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Compositor> listarCompositor() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        ICompositorDAO dao = factory.getCompositorDAO();
        return dao.listarCompositor();
    }
}
