package saenz.marcos.bl.logica;

import saenz.marcos.DAO.DAOFactory;
import saenz.marcos.bl.entities.Genero.Genero;
import saenz.marcos.bl.entities.Genero.IGeneroDAO;
import saenz.marcos.dl.DataGenero;
import saenz.marcos.utils.Utilities;

import java.util.ArrayList;

public class GestorGenero {

    private DataGenero dataGenero;

    public GestorGenero(){
        dataGenero = new DataGenero();
    }

    public boolean buscarGenero(String nombre){
        return dataGenero.buscarGenero(nombre);
    }

    public void registrarGenero(String nombre, String descripcion) throws Exception {
        try {
            Genero genero = new Genero(nombre, descripcion);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            IGeneroDAO dao = factory.getGeneroDAO();
            dao.registrarGenero(genero);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }


    public ArrayList<Genero> listarGeneros() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IGeneroDAO dao = factory.getGeneroDAO();
        return dao.listarGenero();
    }
}
