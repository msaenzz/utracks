package saenz.marcos.bl.logica;

import saenz.marcos.DAO.DAOFactory;
import saenz.marcos.bl.entities.Administrador.Administrador;
import saenz.marcos.bl.entities.Administrador.IAdministradorDAO;
import saenz.marcos.bl.entities.General.General;
import saenz.marcos.bl.entities.General.IGeneralDAO;
import saenz.marcos.bl.entities.Usuario.IUsuarioDAO;
import saenz.marcos.bl.entities.Usuario.Usuario;
import saenz.marcos.dl.DataUsuario;
import saenz.marcos.utils.Utilities;

import java.util.ArrayList;

public class GestorUsuario {

    private DataUsuario dataUsuario;

    public GestorUsuario() {
        dataUsuario = new DataUsuario();
    }

    public void registrarUsuario(String id, String nombre, String apellidoUno, String apellidoDos, String correoElectronico,
                                 String nombreUsuario, String avatar, String tipo) throws Exception {
        try {
            String cod = dataUsuario.codigo();
            Usuario usuario = new Usuario(id, nombre, apellidoUno, apellidoDos, correoElectronico, nombreUsuario, cod, avatar,tipo);
            dataUsuario.enviarC(usuario, cod);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            IUsuarioDAO dao = factory.getUsuarioDAO();
            dao.registrarUsuario(usuario);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarAdministrador(String id) throws Exception {
        try {
            Administrador administrador = new Administrador(id);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            IAdministradorDAO dao = factory.getAdministradorDAO();
            dao.registrarAdministrador(administrador);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarGeneral(String paisNacimiento, int edad, String id) throws Exception {
        try {
            General general = new General(paisNacimiento, edad, id);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            IGeneralDAO dao = factory.getGeneralDAO();
            dao.registrarGeneral(general);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //registra al usuario logeado
    public boolean logearUsuario(String correo, String pass) throws Exception {
        {
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            IUsuarioDAO dao = factory.getUsuarioDAO();
            return dataUsuario.crearUsuario(dao.extraerUsuario(correo, pass));
        }
    }

    //verifica que exista un admi en la aplicacion
    public ArrayList<Administrador> verificarAdministrador() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IAdministradorDAO dao = factory.getAdministradorDAO();
        return dao.listarAdministrador();
    }

    //trae los datos del usuario logeado
    public ArrayList<Usuario> infoUsuarioLog() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IUsuarioDAO dao = factory.getUsuarioDAO();
        if(!dataUsuario.infoUsuario().equals(""))
            return dao.extraerUsuario(dataUsuario.infoUsuario());
        return new ArrayList<>();
    }

    //cuando se cierre sesión
    public void cerrarSesion(){
        dataUsuario.eliminarUsuario();
    }

    //modificar la contrasenna
    public void modificarContra(String pass, String correo) throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IUsuarioDAO dao = factory.getUsuarioDAO();
        dao.nuevaContr(pass, correo);
    }

    //modificar la contrasenna
    public boolean recuperarContrasenna(String correo) throws Exception {
        String cod = dataUsuario.codigo();
        Usuario usuario = new Usuario();
        usuario.setCorreoElectronico(correo);
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IUsuarioDAO dao = factory.getUsuarioDAO();
        if(dao.verificarUsuario(usuario.getCorreoElectronico()).size()!=0) {
            usuario = dao.verificarUsuario(usuario.getCorreoElectronico()).get(0);
                dataUsuario.enviarC(usuario, cod);
                dao.nuevaContr(cod, correo);
                return true;
        }else{
            return false;
        }
    }
}
