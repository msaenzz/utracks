package saenz.marcos.bl.logica;

import saenz.marcos.DAO.DAOFactory;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.bl.entities.Artista.IArtistaDAO;
import saenz.marcos.bl.entities.Genero.Genero;
import saenz.marcos.dl.DataArtista;
import saenz.marcos.utils.Utilities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class GestorArtista {
    private DataArtista dataArtista;

    public GestorArtista(){
        dataArtista = new DataArtista();
    }

    public void registrarArtista(String nombre, String apellido, String nombreArtistico, LocalDate fechaNacimiento, LocalDate fechaDefuncion, Genero genero, int edad, String descripcion, String paisNacimiento) throws Exception {
        try {
            Artista artista = new Artista(nombre, apellido,nombreArtistico,fechaNacimiento,fechaDefuncion,genero,edad,descripcion,paisNacimiento);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            IArtistaDAO dao = factory.getArtistaDAO();
            dao.registrrarArtista(artista);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Artista> listarArtistas() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IArtistaDAO dao = factory.getArtistaDAO();
        return dao.listarArtistas();
    }
}
