package saenz.marcos.bl.logica;

import saenz.marcos.DAO.DAOFactory;
import saenz.marcos.bl.entities.Album.Album;
import saenz.marcos.bl.entities.Album.IAlbumDAO;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.dl.DataAlbum;
import saenz.marcos.utils.Utilities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class GestorAlbum {

    private DataAlbum dataAlbum;

    public GestorAlbum(){
        dataAlbum = new DataAlbum();
    }

    public boolean buscarAlbum(String nombre){
        return dataAlbum.buscarAlbum(nombre);
    }

    public void registrarAlbum(String nombre, LocalDate fechaLanzamiento, Artista artista, String imgAlbum){
        try {
            Album album = new Album(nombre, fechaLanzamiento,artista,imgAlbum);
            DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
            IAlbumDAO dao = factory.getAlbumDAO();
            dao.registrarAlbum(album);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Album> listarAlbums() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IAlbumDAO dao = factory.getAlbumDAO();
        return dao.listarAlbum();
    }
}
