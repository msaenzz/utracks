package saenz.marcos.bl.logica;

import saenz.marcos.DAO.DAOFactory;
import saenz.marcos.bl.entities.pais.IPaisDAO;
import saenz.marcos.bl.entities.pais.Pais;
import saenz.marcos.utils.Utilities;

import java.util.ArrayList;

public class GestorPais {

    //verifica que exista un admi en la aplicacion
    public ArrayList<Pais> listarPaises() throws Exception {
        DAOFactory factory = DAOFactory.getDAOFactory(Integer.parseInt(Utilities.getMotorDB()));
        IPaisDAO dao = factory.getPaisDAO();
        return dao.listarPaises();
    }
}
