package saenz.marcos.bl.entities.Usuario;

import java.util.ArrayList;

public interface IUsuarioDAO {
    void registrarUsuario(Usuario u) throws Exception;
    ArrayList<Usuario> extraerUsuario(String correo, String pass) throws Exception;
    ArrayList<Usuario> extraerUsuario(String id) throws Exception;
    ArrayList<Usuario> verificarUsuario(String correo) throws Exception;
    void nuevaContr(String pass, String correo) throws Exception;
}
