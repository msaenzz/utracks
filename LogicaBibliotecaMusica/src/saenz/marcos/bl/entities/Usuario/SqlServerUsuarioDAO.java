package saenz.marcos.bl.entities.Usuario;


import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerUsuarioDAO implements IUsuarioDAO {

    @Override
    public void registrarUsuario(Usuario u) throws Exception {
        try {
            String query = "INSERT INTO USUARIO(id,nombre,apellidoUno,apellidoDos,correoElectronico,nombreUsuario,contrasenna,avatar,tipo) " +
                    "VALUES ('" + u.getId() + "','" + u.getNombre() + "','" + u.getApellidoUno() + "','" + u.getApellidoDos()
                    + "','" + u.getCorreoElectronico() + "','" + u.getNombreUsuario() + "','" + u.getContrasenna()
                    + "','" + u.getAvatar() + "','" + u.getTipo() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }

    @Override
    public ArrayList<Usuario> extraerUsuario(String correo, String pass) throws Exception {
        ArrayList<Usuario> us = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "select id,nombre,apellidoUno,apellidoDos,correoElectronico,nombreUsuario,contrasenna,avatar,tipo" +
                    " from Usuario where correoElectronico = '" + correo + "' and contrasenna = '" + pass + "';";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (us == null) {
                us = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    Usuario tmpusuario = new Usuario(rs.getString("id"),
                            rs.getString("nombre"),
                            rs.getString("apellidoUno"),
                            rs.getString("apellidoDos"),
                            rs.getString("correoElectronico"),
                            rs.getString("nombreUsuario"),
                            rs.getString("contrasenna"),
                            rs.getString("avatar"),
                            rs.getString("tipo"));

                    us.add(tmpusuario);
                }
            }
            return us;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ArrayList<Usuario> extraerUsuario(String id) throws Exception {
        ArrayList<Usuario> us = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "select id,nombre,apellidoUno,apellidoDos,correoElectronico,nombreUsuario,contrasenna,avatar,tipo" +
                    " from Usuario where id = '" + id + "';";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (us == null) {
                us = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    Usuario tmpusuario = new Usuario(rs.getString("id"),
                            rs.getString("nombre"),
                            rs.getString("apellidoUno"),
                            rs.getString("apellidoDos"),
                            rs.getString("correoElectronico"),
                            rs.getString("nombreUsuario"),
                            rs.getString("contrasenna"),
                            rs.getString("avatar"),
                            rs.getString("tipo"));

                    us.add(tmpusuario);
                }
            }
            return us;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ArrayList<Usuario> verificarUsuario(String correo) throws Exception {
        ArrayList<Usuario> us = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "select id,nombre,apellidoUno,apellidoDos,correoElectronico,nombreUsuario,contrasenna,avatar,tipo" +
                    " from Usuario where correoElectronico = '" + correo + "';";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (us == null) {
                us = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    Usuario tmpusuario = new Usuario(rs.getString("id"),
                            rs.getString("nombre"),
                            rs.getString("apellidoUno"),
                            rs.getString("apellidoDos"),
                            rs.getString("correoElectronico"),
                            rs.getString("nombreUsuario"),
                            rs.getString("contrasenna"),
                            rs.getString("avatar"),
                            rs.getString("tipo"));

                    us.add(tmpusuario);
                }
            }
            return us;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void nuevaContr(String pass, String correo) throws Exception {
        try {
            String query = "UPDATE USUARIO SET contrasenna = '" + pass + "' WHERE correoElectronico = '" + correo + "'";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }
}
