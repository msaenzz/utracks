package saenz.marcos.bl.entities.Artista;

import saenz.marcos.bl.entities.Genero.Genero;

import java.time.LocalDate;
import java.util.Date;

public class Artista {
    private String id;
    private String nombre;
    private String apellido;
    private  String nombreArtistico;
    private LocalDate fechaNacimiento;
    private  LocalDate fechaDefuncion;
    private Genero idGenero;
    private int edad;
    private String descripcion;
    private  String paisNacimiento;

    public Artista() {
    }

    public Artista(String id, String nombre, String apellido, String nombreArtistico, LocalDate fechaNacimiento,
                   LocalDate fechaDefuncion, Genero genero, int edad, String descripcion, String paisNacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.nombreArtistico = nombreArtistico;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaDefuncion = fechaDefuncion;
        this.idGenero = genero;
        this.edad = edad;
        this.descripcion = descripcion;
        this.paisNacimiento = paisNacimiento;
    }

    public Artista(String nombre, String apellido, String nombreArtistico, LocalDate fechaNacimiento, LocalDate fechaDefuncion, Genero genero, int edad, String descripcion, String paisNacimiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nombreArtistico = nombreArtistico;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaDefuncion = fechaDefuncion;
        this.idGenero = genero;
        this.edad = edad;
        this.descripcion = descripcion;
        this.paisNacimiento = paisNacimiento;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombreArtistico() {
        return nombreArtistico;
    }

    public void setNombreArtistico(String nombreArtistico) {
        this.nombreArtistico = nombreArtistico;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public LocalDate getFechaDefuncion() {
        return fechaDefuncion;
    }

    public void setFechaDefuncion(LocalDate fechaDefuncion) {
        this.fechaDefuncion = fechaDefuncion;
    }

    public Genero getGenero() {
        return idGenero;
    }

    public void setGenero(Genero genero) {
        this.idGenero = genero;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPaisNacimiento() {
        return paisNacimiento;
    }

    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;
    }

    @Override
    public String toString() {
        return "Artista{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", nombreArtistico='" + nombreArtistico + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", fechaDefuncion=" + fechaDefuncion +
                ", genero='" + idGenero + '\'' +
                ", edad=" + edad +
                ", descripcion='" + descripcion + '\'' +
                ", paisNacimiento='" + paisNacimiento + '\'' +
                '}';
    }
}
