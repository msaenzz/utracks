package saenz.marcos.bl.entities.Artista;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IArtistaDAO {
    void registrrarArtista(Artista art) throws SQLException;
    ArrayList<Artista> listarArtistas() throws Exception;
}
