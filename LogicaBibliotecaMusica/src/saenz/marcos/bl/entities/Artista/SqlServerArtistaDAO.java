package saenz.marcos.bl.entities.Artista;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.bl.entities.Genero.Genero;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerArtistaDAO implements IArtistaDAO {
    @Override
    public void registrrarArtista(Artista art) throws SQLException {
        try {
            String query = "INSERT INTO Artista(nombre,apellido,nombreArtistico,fechaNacimiento,fechaDefuncion,idGenero,edad,descripcion,paisNacimiento) " +
                    "VALUES ('" + art.getNombre() + "','" + art.getApellido() + "','" + art.getNombreArtistico() + "','" + art.getFechaNacimiento()
                    + "','" + art.getFechaDefuncion() + "','" + art.getGenero().getId() + "','" + art.getEdad() + "','" + art.getDescripcion() + "','" + art.getPaisNacimiento() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }

    @Override
    public ArrayList<Artista> listarArtistas() throws Exception {
        ArrayList<Artista> artista = null;
        ResultSet rs = null;
        Genero gen = new Genero();
        try {
            AccesoBD accesoDatos;
            String query = "select id,nombre,apellido,nombreArtistico,fechaNacimiento,fechaDefuncion,idGenero,edad,descripcion,paisNacimiento" +
                    " from Artista;";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (artista == null) {
                artista = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    //setea el objeto genero primero
                    gen.setId(rs.getString("idGenero"));
                    //llena al objeto artista
                    Artista tmpartista = new Artista(rs.getString("id"),
                            rs.getString("nombre"),
                            rs.getString("apellido"),
                            rs.getString("nombreArtistico"),
                            LocalDate.parse(rs.getString("fechaNacimiento")),
                            LocalDate.parse(rs.getString("fechaDefuncion")),
                            gen,
                            rs.getInt("edad"),
                            rs.getString("descripcion"),
                            rs.getString("paisNacimiento"));

                    artista.add(tmpartista);
                }
            }
            return artista;
        } catch (Exception e) {
            throw e;
        }
    }
}
