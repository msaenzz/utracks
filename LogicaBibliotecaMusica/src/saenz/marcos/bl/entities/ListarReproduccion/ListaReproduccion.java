package saenz.marcos.bl.entities.ListarReproduccion;

import saenz.marcos.bl.entities.Cancion.Cancion;

import java.util.ArrayList;

public class ListaReproduccion {
    private String id;
    private ArrayList<Cancion> canciones;
    private String nombre;
    private String calificacion;

    public ListaReproduccion() {
    }

    public ListaReproduccion(String id, ArrayList<Cancion> canciones, String nombre, String calificacion) {
        this.id = id;
        this.canciones = canciones;
        this.nombre = nombre;
        this.calificacion = calificacion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(ArrayList<Cancion> canciones) {
        this.canciones = canciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "ListaReproduccion{" +
                "id=" + id +
                ", canciones=" + canciones +
                ", nombre='" + nombre + '\'' +
                ", calificacion='" + calificacion + '\'' +
                '}';
    }
}
