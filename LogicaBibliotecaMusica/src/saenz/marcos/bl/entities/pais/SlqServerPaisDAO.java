package saenz.marcos.bl.entities.pais;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.util.ArrayList;

public class SlqServerPaisDAO implements IPaisDAO {

    @Override
    public ArrayList<Pais> listarPaises() throws Exception {
        ArrayList<Pais> pais = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "select id,nombre,iso from Paises;";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (pais == null) {
                pais = new ArrayList<>();
            }
            if(rs!=null) {
                while (rs.next()) {
                    Pais tmppais = new Pais(rs.getString("id"),
                            rs.getString("nombre"),
                            rs.getString("iso"));

                    pais.add(tmppais);
                }
            }
            return pais;
        } catch (Exception e) {
            throw e;
        }
    }
}
