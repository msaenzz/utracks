package saenz.marcos.bl.entities.pais;

import java.util.ArrayList;

public interface IPaisDAO {
    ArrayList<Pais> listarPaises() throws Exception;
}
