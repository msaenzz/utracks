package saenz.marcos.bl.entities.pais;

public class Pais {
    private String id;
    private String nombre;
    private String iso;

    public Pais(String id, String nombre, String iso) {
        this.id = id;
        this.nombre = nombre;
        this.iso = iso;
    }

    public Pais() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    @Override
    public String toString() {
        return "Pais{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", iso='" + iso + '\'' +
                '}';
    }
}
