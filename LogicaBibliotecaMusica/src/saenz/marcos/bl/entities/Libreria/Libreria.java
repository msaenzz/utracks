package saenz.marcos.bl.entities.Libreria;

import saenz.marcos.bl.entities.Cancion.Cancion;
import saenz.marcos.bl.entities.Usuario.Usuario;

import java.util.ArrayList;

public class Libreria {
    private  ArrayList<Cancion> canciones;
    private Usuario usuario;
    private  String id;


    public Libreria() {
    }

    public Libreria(ArrayList<Cancion> canciones, Usuario usuario, String id) {
        this.canciones = canciones;
        this.usuario = usuario;
        this.id = id;
    }

    public ArrayList<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(ArrayList<Cancion> canciones) {
        this.canciones = canciones;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Libreria{" +
                "canciones=" + canciones +
                ", usuario=" + usuario +
                ", id=" + id +
                '}';
    }
}
