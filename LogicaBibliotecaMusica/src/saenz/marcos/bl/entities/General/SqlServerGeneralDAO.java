package saenz.marcos.bl.entities.General;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerGeneralDAO implements IGeneralDAO {
    @Override
    public ArrayList<General> listarGeneral() throws Exception {
        ArrayList<General> generals = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            while (rs.next()) {
                General tmpgeneral = new General(rs.getString("paisNacimiento"),
                        rs.getInt("edad"),
                        rs.getString("id"),
                        rs.getString("nombre"),
                        rs.getString("apellidoUno"),
                        rs.getString("apellidoDos"),
                        rs.getString("correoElectronico"),
                        rs.getString("nombreUsuario"),
                        rs.getString("contrasenna"),
                        rs.getString("avatar"),
                        rs.getString("tipo"));
                if (generals == null) {
                    generals = new ArrayList<>();
                }
                generals.add(tmpgeneral);
            }
            return generals;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void registrarGeneral(General g) throws Exception {
        try {
            String query = "INSERT INTO GENERAL(idUsuario,paisNacimiento,edad) VALUES ('"
                    + g.getId() + "','" + g.getPaisNacimiento() + "','" + g.getEdad() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }
}
