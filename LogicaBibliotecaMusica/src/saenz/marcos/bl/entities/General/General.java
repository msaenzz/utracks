package saenz.marcos.bl.entities.General;

import saenz.marcos.bl.entities.Usuario.Usuario;

public class General extends Usuario {
    private String paisNacimiento;
    private int edad;

    public General(String paisNacimiento, int edad, String id, String nombre, String apellidoUno, String apellidoDos,
                   String correoElectronico, String nombreUsuario, String contrasenna, String avatar, String tipo) {
        super(id, nombre, apellidoUno, apellidoDos, correoElectronico, nombreUsuario, contrasenna, avatar, tipo);
        this.paisNacimiento = paisNacimiento;
        this.edad = edad;
    }

    public General() {
    }

    public General(String paisNacimiento, int edad, String id) {
        super(id);
        this.paisNacimiento = paisNacimiento;
        this.edad = edad;
    }

    public String getPaisNacimiento() {
        return paisNacimiento;
    }

    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "General{" +
                "paisNacimiento='" + paisNacimiento + '\'' +
                ", edad=" + edad +
                "} " + super.toString();
    }
}
