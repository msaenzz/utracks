package saenz.marcos.bl.entities.General;

import java.util.ArrayList;

public interface IGeneralDAO {
    ArrayList<General> listarGeneral() throws Exception;
    void registrarGeneral(General u) throws Exception;
}
