package saenz.marcos.bl.entities.Administrador;

import saenz.marcos.bl.entities.Usuario.Usuario;

public class Administrador extends Usuario {

    public Administrador() {
    }

    public Administrador(String id) {
        super(id);
    }

    public Administrador(String id, String nombre, String apellidoUno, String apellidoDos, String correoElectronico,
                         String nombreUsuario, String contrasenna, String avatar,String tipo) {
       super( id,  nombre,  apellidoUno,  apellidoDos, correoElectronico,  nombreUsuario,  contrasenna, avatar, tipo);
    }

    @Override
    public String toString() {
        return "Administrador{} " + super.toString();
    }
}
