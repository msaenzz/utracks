package saenz.marcos.bl.entities.Administrador;

import java.util.ArrayList;

public interface IAdministradorDAO {

    ArrayList<Administrador> listarAdministrador() throws Exception;
    void registrarAdministrador(Administrador a) throws Exception;
    void actualizarAdministrador(Administrador a) throws Exception;
    void borrarAdministrador(String id) throws Exception;

}
