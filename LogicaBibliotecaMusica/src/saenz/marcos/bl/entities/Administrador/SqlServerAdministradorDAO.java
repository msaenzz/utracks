package saenz.marcos.bl.entities.Administrador;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerAdministradorDAO implements IAdministradorDAO{

    @Override
    public void registrarAdministrador(Administrador a) throws Exception {
        try {
            String query = "INSERT INTO ADMINISTRADOR(idUsuario) VALUES ('"
                    + a.getId() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }

    @Override
    public ArrayList<Administrador> listarAdministrador() throws Exception {
        ArrayList<Administrador> us = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "select id from Administrador;";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (us == null) {
                us = new ArrayList<>();
            }
            if(rs!=null) {
                while (rs.next()) {
                    Administrador tmpusuario = new Administrador(rs.getString("id"));
                    us.add(tmpusuario);
                }
            }
            return us;
        } catch (Exception e) {
            throw e;
        }
    }


    @Override
    public void actualizarAdministrador(Administrador a) throws Exception {

    }

    @Override
    public void borrarAdministrador(String id) throws Exception {

    }
}
