package saenz.marcos.bl.entities.Album;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IAlbumDAO {
    void registrarAlbum(Album alb) throws SQLException;
    ArrayList<Album> listarAlbum() throws Exception;
}
