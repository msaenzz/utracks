package saenz.marcos.bl.entities.Album;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerAlbumDAO implements IAlbumDAO{
    @Override
    public void registrarAlbum(Album alb) throws SQLException {
        try {
            String query = "INSERT INTO ALBUM(nombre,fechaLanzamiento,idArtista,imagenAlbum) " +
                    "VALUES ('" + alb.getNombre() + "','" + alb.getFechaLanzamiento() + "','" +
                    alb.getArtista().getId() + "','" + alb.getImagenAlbum() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }

    @Override
    public ArrayList<Album> listarAlbum() throws Exception {
        ArrayList<Album> albums = null;
        ResultSet rs = null;
        Artista art = new Artista();
        try {
            AccesoBD accesoDatos;
            String query = "Select id, nombre,fechaLanzamiento,idArtista,imagenAlbum" +
                    " from Album;";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (albums == null) {
                albums = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    //setea el objeto genero primero
                    art.setId(rs.getString("idArtista"));
                    //llena al objeto cancion
                    Album tmpcancion = new Album(rs.getString("id"),
                            rs.getString("nombre"),
                            LocalDate.parse(rs.getString("fechaLanzamiento")),
                            art,
                            rs.getString("imagenAlbum"));

                    albums.add(tmpcancion);
                }
            }
            return albums;
        } catch (Exception e) {
            throw e;
        }
    }
}
