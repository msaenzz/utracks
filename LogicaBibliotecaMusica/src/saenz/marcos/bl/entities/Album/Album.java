package saenz.marcos.bl.entities.Album;

import saenz.marcos.bl.entities.Artista.Artista;

import java.time.LocalDate;
import java.util.Date;

public class Album {
    private String id;
    private String nombre;
    private LocalDate fechaLanzamiento;
    private Artista idArtista;
    private String imagenAlbum;

    public Album() {
    }

    public Album(String id,String nombre, LocalDate fechaLanzamiento, Artista artista, String imagenAlbum) {
        this.id = id;
        this.nombre = nombre;
        this.fechaLanzamiento = fechaLanzamiento;
        this.idArtista = artista;
        this.imagenAlbum = imagenAlbum;
    }

    public Album(String id,String nombre, LocalDate fechaLanzamiento, Artista artista) {
        this.id = id;
        this.nombre = nombre;
        this.fechaLanzamiento = fechaLanzamiento;
        this.idArtista = artista;

    }

    public Album(String nombre, LocalDate fechaLanzamiento, Artista artista, String imagenAlbum) {
        this.nombre = nombre;
        this.fechaLanzamiento = fechaLanzamiento;
        this.idArtista = artista;
        this.imagenAlbum = imagenAlbum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    public void setFechaLanzamiento(LocalDate fechaLanzamiento) {
        this.fechaLanzamiento = fechaLanzamiento;
    }

    public Artista getArtista() {
        return idArtista;
    }

    public void setArtista(Artista artista) {
        this.idArtista = artista;
    }

    public String getImagenAlbum() {
        return imagenAlbum;
    }

    public void setImagenAlbum(String imagenAlbum) {
        this.imagenAlbum = imagenAlbum;
    }

    @Override
    public String toString() {
        return "Album{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", FechaLanzamiento=" + fechaLanzamiento +
                ", artista=" + idArtista +
                ", imagenAlbum='" + imagenAlbum + '\'' +
                '}';
    }
}
