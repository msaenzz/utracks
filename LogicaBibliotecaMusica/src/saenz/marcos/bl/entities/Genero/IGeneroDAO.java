package saenz.marcos.bl.entities.Genero;

import java.util.ArrayList;

public interface IGeneroDAO {
    ArrayList<Genero> listarGenero() throws Exception;
    void registrarGenero(Genero g) throws Exception;
}
