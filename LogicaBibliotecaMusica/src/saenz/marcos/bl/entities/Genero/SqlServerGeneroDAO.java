package saenz.marcos.bl.entities.Genero;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerGeneroDAO implements IGeneroDAO {
    @Override
    public ArrayList<Genero> listarGenero() throws Exception {
        ArrayList<Genero> genero = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "select id, nombre, descripcion from genero;";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            while (rs.next()) {
                Genero tmpgenero = new Genero(rs.getString("id"),
                        rs.getString("nombre"),
                        rs.getString("descripcion"));
                if (genero == null) {
                    genero = new ArrayList<>();
                }
                genero.add(tmpgenero);
            }
            return genero;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void registrarGenero(Genero g) throws Exception {
        try {
            String query = "INSERT INTO GENERO(nombre,descripcion) VALUES ('"
                    + g.getNombre() + "','" + g.getDescripcion() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }
}
