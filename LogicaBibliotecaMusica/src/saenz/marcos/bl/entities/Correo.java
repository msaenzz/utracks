package saenz.marcos.bl.entities;

import saenz.marcos.bl.entities.Usuario.Usuario;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.awt.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Correo {
    public Correo() {
    }

    public void sendEmails(Usuario customer, String cod) {
        Properties propiedad = new Properties();
        propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
        propiedad.setProperty("mail.smtp.starttls.enable", "true");
        propiedad.setProperty("mail.smtp.port", "587");
        propiedad.setProperty("mail.smtp.auth", "true");


        Session sesion = Session.getDefaultInstance(propiedad);
        // email emisor
        String correoEnvia = "utrackspoo2020@gmail.com";
        String contrasena = "UTracks2020";
        // destinatario email
        String receptor = customer.getCorreoElectronico();
        String asunto = "Contraseña de acceso";
        String mensaje =  "U-Tracks se hace presente por este medio para mostrarle a " +
                " Sr./Sra. " + customer.getNombre() + " " + customer.getApellidoUno() + " nuestra contraseña de acceso: "
                + cod + "\n"  + "\n Muchas gracias \n  Se despide \n U-Tracks Inc";

        MimeMessage mail = new MimeMessage(sesion);
        try {
            mail.setFrom(new InternetAddress(correoEnvia));
            mail.addRecipient(Message.RecipientType.TO, new InternetAddress(receptor));
            mail.setSubject(asunto);
            mail.setText(mensaje);

            Transport transportar = sesion.getTransport("smtp");
            transportar.connect(correoEnvia, contrasena);
            transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
            transportar.close();
        } catch (AddressException ex) {
            Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


