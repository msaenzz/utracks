package saenz.marcos.bl.entities.Cancion;

import saenz.marcos.bl.entities.Album.Album;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.bl.entities.Compositor.Compositor;
import saenz.marcos.bl.entities.Genero.Genero;

import java.time.LocalDate;
import java.util.Date;

public class Cancion {

    private String id;
    private Genero genero;
    private Artista artista;
    private Compositor compositor;
    private Album album;
    private LocalDate fechaLanzamiento;
    private String nombre;
    private String url;

    public Cancion() {
    }

    public Cancion(String id, String nombre, Genero genero, Artista artista, Compositor compositor, Album album, LocalDate fechaLanzamiento,String url) {
        this.id = id;
        this.nombre = nombre;
        this.genero = genero;
        this.artista = artista;
        this.compositor = compositor;
        this.album = album;
        this.fechaLanzamiento = fechaLanzamiento;
        this.url = url;
    }

    public Cancion(Genero genero, Artista artista, Compositor compositor, Album album, LocalDate fechaLanzamiento, String nombre, String url) {
        this.genero = genero;
        this.artista = artista;
        this.compositor = compositor;
        this.album = album;
        this.fechaLanzamiento = fechaLanzamiento;
        this.nombre = nombre;
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }

    public Compositor getCompositor() {
        return compositor;
    }

    public void setCompositor(Compositor compositor) {
        this.compositor = compositor;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public LocalDate getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    public void setFechaLanzamiento(LocalDate fechaLanzamiento) {
        this.fechaLanzamiento = fechaLanzamiento;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return url;
    }
}
