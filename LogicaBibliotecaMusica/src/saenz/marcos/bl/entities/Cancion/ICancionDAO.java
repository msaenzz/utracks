package saenz.marcos.bl.entities.Cancion;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ICancionDAO {
    void registrarCancion(Cancion cancion) throws SQLException;
    ArrayList<Cancion> listarCanciones() throws Exception;
    ArrayList<Cancion> listarBiblioteca() throws Exception;
}
