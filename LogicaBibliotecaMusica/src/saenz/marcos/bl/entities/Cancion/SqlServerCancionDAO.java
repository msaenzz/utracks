package saenz.marcos.bl.entities.Cancion;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.bl.entities.Album.Album;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.bl.entities.Compositor.Compositor;
import saenz.marcos.bl.entities.Genero.Genero;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerCancionDAO implements ICancionDAO {
    @Override
    public void registrarCancion(Cancion can) throws SQLException {
        try {
            String query = "INSERT INTO Cancion(genero,artista,compositor,album,fechaLanzamiento,nombre,url) " +
                    "VALUES ('" + can.getGenero().getId() + "','" + can.getArtista().getId() + "','" +
                    can.getCompositor().getId() + "','" + can.getAlbum().getId()
                    + "','" + can.getFechaLanzamiento() + "','" + can.getNombre() + "','" + can.getUrl() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }

    @Override
    public ArrayList<Cancion> listarCanciones() throws Exception {
        ArrayList<Cancion> cancion = null;
        ResultSet rs = null;
        Genero gen = new Genero();
        Artista art = new Artista();
        Compositor com = new Compositor();
        Album alb = new Album();
        try {
            AccesoBD accesoDatos;
            String query = "Select id,genero,artista,compositor,album,fechaLanzamiento,nombre,url" +
                    " from Cancion;";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (cancion == null) {
                cancion = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    //setea el objeto genero primero
                    gen.setId(rs.getString("genero"));
                    art.setId(rs.getString("artista"));
                    com.setId(rs.getString("compositor"));
                    alb.setId(rs.getString("album"));
                    //llena al objeto cancion
                    Cancion tmpcancion = new Cancion(rs.getString("id"),
                            rs.getString("nombre"),
                            gen,
                            art,
                            com,
                            alb,
                            LocalDate.parse(rs.getString("fechaLanzamiento")),
                            rs.getString("url"));

                    cancion.add(tmpcancion);
                }
            }
            return cancion;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ArrayList<Cancion> listarBiblioteca() throws Exception {
        ArrayList<Cancion> cancion = null;
        ResultSet rs = null;
        Genero gen = new Genero();
        Artista art = new Artista();
        Compositor com = new Compositor();
        Album alb = new Album();
        try {
            AccesoBD accesoDatos;
            String query = "select can.fechaLanzamiento, can.genero, art.nombreArtistico artista, com.nombre compositor, can.album, can.nombre, can.url, can.id " +
                    "from Cancion can " +
                    "inner join Artista art on art.id = can.artista " +
                    "inner join Compositor com on com.id = can.compositor";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (cancion == null) {
                cancion = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    //setea el objeto genero primero
                    gen.setId(rs.getString("genero"));
                    art.setNombreArtistico(rs.getString("artista"));
                    com.setNombre(rs.getString("compositor"));
                    alb.setId(rs.getString("album"));
                    //llena al objeto cancion
                    Cancion tmpcancion = new Cancion(rs.getString("id"),
                            rs.getString("nombre"),
                            gen,
                            art,
                            com,
                            alb,
                            LocalDate.parse(rs.getString("fechaLanzamiento")),
                            rs.getString("url"));

                    cancion.add(tmpcancion);
                }
            }
            return cancion;
        } catch (Exception e) {
            throw e;
        }
    }
}
