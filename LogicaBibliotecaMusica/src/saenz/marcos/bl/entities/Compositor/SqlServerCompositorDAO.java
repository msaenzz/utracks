package saenz.marcos.bl.entities.Compositor;

import cr.ac.ucenfotec.accesodatos.AccesoBD;
import cr.ac.ucenfotec.accesodatos.Conector;
import saenz.marcos.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerCompositorDAO implements ICompositorDAO {
    @Override
    public void registrarCompositor(Compositor com) throws SQLException {
        try {
            String query = "INSERT INTO Compositor(nombre,apellido,paisNacimiento,edad) " +
                    "VALUES ('" + com.getNombre() + "','" + com.getApellido() + "','" + com.getPaisNacimiento() + "','" + com.getEdad() +"')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {

        }
    }

    @Override
    public ArrayList<Compositor> listarCompositor() throws Exception {
        ArrayList<Compositor> com = null;
        ResultSet rs = null;
        try {
            AccesoBD accesoDatos;
            String query = "select id,nombre,apellido,paisNacimiento,edad" +
                    " from Compositor;";
            accesoDatos = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]);
            rs = accesoDatos.ejecutarSQL(query);
            //recorrer el rs
            if (com == null) {
                com = new ArrayList<>();
            }
            if (rs != null) {
                while (rs.next()) {
                    Compositor tmpusuario = new Compositor(rs.getString("id"),
                            rs.getString("nombre"),
                            rs.getString("apellido"),
                            rs.getString("paisNacimiento"),
                            rs.getInt("edad"));

                    com.add(tmpusuario);
                }
            }
            return com;
        } catch (Exception e) {
            throw e;
        }
    }
}
