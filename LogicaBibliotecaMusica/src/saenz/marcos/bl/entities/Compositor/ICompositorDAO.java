package saenz.marcos.bl.entities.Compositor;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ICompositorDAO {
    void registrarCompositor(Compositor compositor) throws SQLException;
    ArrayList<Compositor> listarCompositor() throws Exception;
}
