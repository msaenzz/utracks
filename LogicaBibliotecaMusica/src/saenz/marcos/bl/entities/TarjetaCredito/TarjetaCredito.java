package saenz.marcos.bl.entities.TarjetaCredito;

public class TarjetaCredito {
    private int numeroTarjeta;
    private int ccv;
    private String nombreCompleto;

    public TarjetaCredito() {
    }

    public TarjetaCredito(int numeroTarjeta, int ccv, String nombreCompleto) {
        this.numeroTarjeta = numeroTarjeta;
        this.ccv = ccv;
        this.nombreCompleto = nombreCompleto;
    }

    public int getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(int numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public int getCcv() {
        return ccv;
    }

    public void setCcv(int ccv) {
        this.ccv = ccv;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    @Override
    public String toString() {
        return "TarjetaCredito{" +
                "numeroTarjeta=" + numeroTarjeta +
                ", ccv=" + ccv +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                '}';
    }
}
