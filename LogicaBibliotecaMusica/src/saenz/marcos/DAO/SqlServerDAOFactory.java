package saenz.marcos.DAO;

import saenz.marcos.bl.entities.Administrador.IAdministradorDAO;
import saenz.marcos.bl.entities.Administrador.SqlServerAdministradorDAO;
import saenz.marcos.bl.entities.Album.IAlbumDAO;
import saenz.marcos.bl.entities.Album.SqlServerAlbumDAO;
import saenz.marcos.bl.entities.Artista.IArtistaDAO;
import saenz.marcos.bl.entities.Artista.SqlServerArtistaDAO;
import saenz.marcos.bl.entities.Cancion.ICancionDAO;
import saenz.marcos.bl.entities.Cancion.SqlServerCancionDAO;
import saenz.marcos.bl.entities.Compositor.ICompositorDAO;
import saenz.marcos.bl.entities.Compositor.SqlServerCompositorDAO;
import saenz.marcos.bl.entities.General.IGeneralDAO;
import saenz.marcos.bl.entities.General.SqlServerGeneralDAO;
import saenz.marcos.bl.entities.Genero.IGeneroDAO;
import saenz.marcos.bl.entities.Genero.SqlServerGeneroDAO;
import saenz.marcos.bl.entities.Usuario.IUsuarioDAO;
import saenz.marcos.bl.entities.Usuario.SqlServerUsuarioDAO;
import saenz.marcos.bl.entities.pais.IPaisDAO;
import saenz.marcos.bl.entities.pais.SlqServerPaisDAO;

public class SqlServerDAOFactory extends DAOFactory{
    //retorna todos los SqlServer's de cada clase para poder acceder a sus funciones 'crud'
    @Override
    public IGeneroDAO getGeneroDAO() {
        return new SqlServerGeneroDAO();
    }

    @Override
    public IUsuarioDAO getUsuarioDAO() {
        return new SqlServerUsuarioDAO();
    }

    @Override
    public IGeneralDAO getGeneralDAO() {
        return new SqlServerGeneralDAO();
    }

    @Override
    public IAdministradorDAO getAdministradorDAO() {
        return new SqlServerAdministradorDAO();
    }

    @Override
    public IPaisDAO getPaisDAO() {
        return new SlqServerPaisDAO();
    }

    @Override
    public IArtistaDAO getArtistaDAO() {
        return new SqlServerArtistaDAO();
    }

    @Override
    public ICompositorDAO getCompositorDAO() {
        return new SqlServerCompositorDAO();
    }

    @Override
    public ICancionDAO getCancionDAO() {
        return new SqlServerCancionDAO();
    }

    @Override
    public IAlbumDAO getAlbumDAO() {
        return new SqlServerAlbumDAO();
    }
}
