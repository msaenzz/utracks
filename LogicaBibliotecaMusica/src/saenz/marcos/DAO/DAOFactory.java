package saenz.marcos.DAO;


import saenz.marcos.bl.entities.Administrador.IAdministradorDAO;
import saenz.marcos.bl.entities.Album.IAlbumDAO;
import saenz.marcos.bl.entities.Artista.IArtistaDAO;
import saenz.marcos.bl.entities.Cancion.ICancionDAO;
import saenz.marcos.bl.entities.Compositor.ICompositorDAO;
import saenz.marcos.bl.entities.General.IGeneralDAO;
import saenz.marcos.bl.entities.Genero.IGeneroDAO;
import saenz.marcos.bl.entities.Usuario.IUsuarioDAO;
import saenz.marcos.bl.entities.pais.IPaisDAO;

public abstract class DAOFactory {
    public static int MYSQL=2;
    public static int SQLSERVER=1;
    public static int TEXT_FILE = 3;

    public static DAOFactory getDAOFactory(int whichFactory){
        switch (whichFactory){
            case 1:
                return new SqlServerDAOFactory();
            default:
                return null;
        }
    }
    //interfaces de todas las clases
    public abstract IGeneroDAO getGeneroDAO();
    public abstract IUsuarioDAO getUsuarioDAO();
    public abstract IGeneralDAO getGeneralDAO();
    public abstract IAdministradorDAO getAdministradorDAO();
    public abstract IPaisDAO getPaisDAO();
    public abstract IArtistaDAO getArtistaDAO();
    public abstract ICompositorDAO getCompositorDAO();
    public abstract ICancionDAO getCancionDAO();
    public abstract IAlbumDAO getAlbumDAO();
}
