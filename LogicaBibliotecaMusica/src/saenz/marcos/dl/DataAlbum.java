package saenz.marcos.dl;

import saenz.marcos.bl.entities.Album.Album;

import java.util.ArrayList;

public class DataAlbum {
    ArrayList<Album> albums;

    public DataAlbum(){
        albums = new ArrayList<>();
    }

    public boolean buscarAlbum(String nombre) {
        for (Album obj : albums) {
            //recorre el arreglo en busca del Album
            if(obj.getNombre().equals(nombre))
                return true;
        }
        return false;
    }

    public void crearAlbum(Album Album) {
        albums.add(Album);
    }

    public String imprimirAlbum() {
        String data = "";
        for (Album obj : albums)
            data+= obj.toString() + "\n";
        return data;
    }
}
