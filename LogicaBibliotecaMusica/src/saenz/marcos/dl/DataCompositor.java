package saenz.marcos.dl;

import saenz.marcos.bl.entities.Compositor.Compositor;

import java.util.ArrayList;

public class DataCompositor {
    ArrayList<Compositor> compositores;

    public DataCompositor(){
        compositores = new ArrayList<>();
    }

    public boolean buscarCompositor(String nombre ) {
        for (Compositor obj : compositores) {
            //recorre el arreglo en busca del Compositor
            if(obj.getNombre().equals(nombre))
                return true;
        }
        return false;
    }

    public void crearCompositor(Compositor compositor) {
        compositores.add(compositor);
    }

    public String imprimirCompositor() {
        String data = "";
        for (Compositor obj : compositores)
            data+= obj.toString() + "\n";
        return data;
    }


}
