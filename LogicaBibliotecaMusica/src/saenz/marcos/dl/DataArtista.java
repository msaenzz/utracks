package saenz.marcos.dl;

import saenz.marcos.bl.entities.Artista.Artista;

import java.util.ArrayList;

public class DataArtista {
    ArrayList<Artista> artistas;

    public DataArtista(){
        artistas = new ArrayList<>();
    }

    public boolean buscarArtista(String nombre) {
        for (Artista obj : artistas) {
            //recorre el arreglo en busca del Artista
            if(obj.getNombre().equals(nombre))
                return true;
        }
        return false;
    }

    public void crearArtista(Artista Artista) {
        artistas.add(Artista);
    }

    public String imprimirArtista() {
        String data = "";
        for (Artista obj : artistas)
            data+= obj.toString() + "\n";
        return data;
    }
}
