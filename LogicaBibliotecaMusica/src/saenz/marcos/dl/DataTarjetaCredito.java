package saenz.marcos.dl;

import saenz.marcos.bl.entities.TarjetaCredito.TarjetaCredito;

import java.util.ArrayList;

public class DataTarjetaCredito {
    ArrayList<TarjetaCredito> tarjetasCredito;

    public DataTarjetaCredito(){
        tarjetasCredito = new ArrayList<>();
    }

    public boolean buscarTarjetaCredito(int numeroTarjeta) {
        for (TarjetaCredito obj : tarjetasCredito) {
            //recorre el arreglo en busca del TarjetaCredito
            // == esta bien????
            if(obj.getNumeroTarjeta()==numeroTarjeta)
                return true;
        }
        return false;
    }

    public void crearTarjetaCredito(TarjetaCredito tarjetaCredito) {
        tarjetasCredito.add(tarjetaCredito);
    }

    public String imprimirTarjetaCredito() {
        String data = "";
        for (TarjetaCredito obj : tarjetasCredito)
            data+= obj.toString() + "\n";
        return data;
    }


}
