package saenz.marcos.dl;

import saenz.marcos.bl.entities.Genero.Genero;

import java.util.ArrayList;

public class DataGenero {
    ArrayList<Genero> generos;

    public DataGenero(){
        generos = new ArrayList<>();
    }

    public boolean buscarGenero(String nombre) {
        for (Genero obj : generos) {
            //recorre el arreglo en busca del Genero
            if(obj.getNombre().equals(nombre))
                return true;
        }
        return false;
    }

    public void crearGenero(Genero genero) {
        generos.add(genero);
    }

    public String imprimirGenero(ArrayList<Genero> genero) {
        String data = "";
        for (Genero obj : genero)
            data+= obj.toString() + "\n";
        return data;
    }


}
