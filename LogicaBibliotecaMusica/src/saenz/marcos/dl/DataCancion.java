package saenz.marcos.dl;

import saenz.marcos.bl.entities.Cancion.Cancion;


import java.util.ArrayList;

public class DataCancion {

    ArrayList<Cancion> canciones;

    public DataCancion(){
        canciones = new ArrayList<>();
    }

    public boolean buscarCancion(String nombre) {
        for (Cancion obj : canciones) {
            //recorre el arreglo en busca del usuario
            if(obj.getNombre().equals(nombre))
                return true;
        }
        return false;
    }

    public void crearCancion(Cancion cancion) {
        canciones.add(cancion);
    }

    public String imprimirCancion() {
        String data = "";
        for (Cancion obj : canciones)
            data+= obj.toString() + "\n";
        return data;
    }
}
