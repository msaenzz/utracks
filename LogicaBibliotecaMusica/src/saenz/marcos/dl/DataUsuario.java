package saenz.marcos.dl;

import saenz.marcos.bl.entities.Correo;
import saenz.marcos.bl.entities.Usuario.Usuario;

import java.util.ArrayList;

public class DataUsuario {
    public static String usuarioLogeado;
    ArrayList<Usuario> usuarios;

    public DataUsuario() {
        usuarios = new ArrayList<>();
    }

    //logear usuario
    public boolean crearUsuario(ArrayList<Usuario> usuario) {
        //guarda el id del usuario
        if (usuario.size() != 0) {
            usuarioLogeado = (usuario.get(0)).getId();
            System.out.println(usuarioLogeado);
            return true;
        }
        return false;
    }

    //buscar si hay usuario logeado
    public boolean usuarioLogeado() {
        return usuarioLogeado.isEmpty();
    }

    //retornar informacion de usuario logeado
    public String infoUsuario() {
        if (usuarioLogeado == null) {
            return "";
        } else {
            return usuarioLogeado;
        }
    }

    //borrar usuario logeado
    public void eliminarUsuario() {
        usuarioLogeado = null;
    }

    //
    public String codigo() {
        int codigo;
        return Integer.toString(codigo = (int) (1000000 * Math.random()));
    }

    //llenar datos de correo
    public void enviarC(Usuario usuario, String cod) {
        Correo c = new Correo();
        c.sendEmails(usuario, cod);
    }
}
