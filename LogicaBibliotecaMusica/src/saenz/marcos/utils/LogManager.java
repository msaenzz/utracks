package saenz.marcos.utils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.*;

public class LogManager {
    public static final Logger LOG_LOCAL = Logger.getLogger(LogManager.class.getName());


    public void exception() throws IOException {
        SimpleFormatter simpleFormatter = null;
        Handler fileHandler = null;
        // un ejemplo para un log simple
        LOG_LOCAL.info(LOG_LOCAL.getName());
        //impresión de la fecha.
        LOG_LOCAL.config(LocalDate.now().toString());
        fileHandler = new FileHandler("Log.log",true);
        simpleFormatter = new SimpleFormatter();
        LOG_LOCAL.addHandler(fileHandler);
        fileHandler.setFormatter(simpleFormatter);
        fileHandler.setLevel(Level.ALL);
        LOG_LOCAL.setLevel(Level.ALL);
        LOG_LOCAL.log(Level.FINE, "Inicio del programa");
    }
}
