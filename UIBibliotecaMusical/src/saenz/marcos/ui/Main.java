package saenz.marcos.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import saenz.marcos.utils.LogManager;


public class Main extends Application {

    public static LogManager log = new LogManager();
    @Override
    public void start(Stage primaryStage) throws Exception {
        log.exception();
        //GestorUsuario gu = new GestorUsuario();
        Parent root = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root, 750, 600));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
