package saenz.marcos.tl;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.shape.Box;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.pais.Pais;
import saenz.marcos.bl.logica.GestorCompositor;
import saenz.marcos.bl.logica.GestorPais;
import saenz.marcos.ui.Main;
import saenz.marcos.utils.LogManager;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.*;

public class RegistrarCompositor implements Initializable {

    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXTextField txtApellidoUno;
    @FXML
    private JFXComboBox BoxPais;
    @FXML
    private JFXTextField txtEdad;
    @FXML
    private Text campos;

    public static ArrayList<Pais> pais = new ArrayList<>();
    public static GestorPais gp = new GestorPais();
    public static GestorCompositor gc = new GestorCompositor();
    public static LogManager log = new LogManager();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            autoSelect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Validar campos
    public boolean validarCamposVacios() {
        return !(txtName.getText().equals("") || txtApellidoUno.getText().equals(""));
    }

    //select que llena los campos
    private void autoSelect() throws Exception {
        pais = gp.listarPaises();
        for (Pais obj : pais) {
            BoxPais.getItems().addAll(obj.getNombre());
        }
    }

    public void registroBT(ActionEvent event) throws Exception {
        try {
            if (validarCamposVacios()) {
                campos.setVisible(false);
                gc.registrarCompositor(txtName.getText(),txtApellidoUno.getText(),BoxPais.getValue().toString(),Integer.parseInt(txtEdad.getText()));
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoCompositor.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Listado compositor");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            } else {
                campos.setVisible(true);
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }

    public void listarCompositorBT(ActionEvent event) throws IOException {
        //Cambiar de escena al Lisatdo compositor
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoCompositor.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Listado compositor");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }
}
