package saenz.marcos.tl;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import saenz.marcos.bl.logica.GestorUsuario;
import saenz.marcos.ui.Main;
import saenz.marcos.utils.LogManager;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.*;

public class Login implements Initializable {

    @FXML
    private JFXTextField txtEmail;
    @FXML
    private JFXPasswordField txtPassword;
    @FXML
    private Text campos;

    public static GestorUsuario gu = new GestorUsuario();
    public static LogManager log = new LogManager();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    //Validar campos
    public boolean validarCamposVacios() {
        return !(txtEmail.getText().equals("") || txtPassword.getText().equals(""));
    }

    public void register(ActionEvent event) throws Exception {
        try {
            if (gu.verificarAdministrador().size() == 0) {
                //Cambiar de escena a registro de admi si no encuentra ningun registro
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarUsuarioA.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Registro");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            } else {
                //Cambiar de escena a registro general si ya encuentra un administrador en la aplicacion
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarUsuarioG.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Registro");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }

    public void passLogiword(ActionEvent event) throws IOException {
        //Cambiar de escena a recuerpara contrasenna
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RecuperarContrasena.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registro");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void ingresar(ActionEvent event) throws Exception {
        try {
            if (validarCamposVacios()) {
                campos.setVisible(false);
                if (gu.logearUsuario(txtEmail.getText(), txtPassword.getText())) {
                    campos.setVisible(false);
                    //Cambiar de escena
                    Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Home.fxml"));
                    Scene changeScene = new Scene(changeToLis);

                    thisWindow.setTitle("Registro");
                    thisWindow.setScene(changeScene);
                    thisWindow.show();
                } else {
                    campos.setText("Datos incorrectos");
                    campos.setVisible(true);
                }
            } else {
                campos.setVisible(true);
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }
}
