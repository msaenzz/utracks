package saenz.marcos.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.Usuario.Usuario;
import saenz.marcos.bl.logica.GestorUsuario;
import saenz.marcos.ui.Main;
import saenz.marcos.utils.LogManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.*;

public class Perfil implements Initializable {

    @FXML
    private ImageView imagen;
    @FXML
    private Text txtUsername;
    @FXML
    private Text txtName;
    @FXML
    private Text txtId;
    @FXML
    private Text txtEmail;
    @FXML
    private Text txtApe1;
    @FXML
    private Text txtApe2;
    @FXML
    private Text txtTipo;
    @FXML
    private Text campos;
    @FXML
    private TextField anterContra;
    @FXML
    private TextField nuevaContr;

    public GestorUsuario gu = new GestorUsuario();
    ArrayList<Usuario> myUser = new ArrayList<>();
    private Image image;
    public static LogManager log = new LogManager();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            myUser = gu.infoUsuarioLog();
            llenarInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void llenarInfo() {
        for (Usuario obj : myUser) {
            txtUsername.setText(obj.getNombreUsuario());
            txtName.setText(obj.getNombre());
            txtApe1.setText(obj.getApellidoUno());
            txtApe2.setText(obj.getApellidoDos());
            txtEmail.setText(obj.getCorreoElectronico());
            txtId.setText(obj.getId());
            txtTipo.setText(obj.getTipo());
            image = new Image(new File("src/saenz/marcos/ui/img/perfil/" + obj.getAvatar()).toURI().toString());
            imagen.setImage(image);
        }
    }


    public void perfilBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de perfil
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Perfil.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Perfil");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tiendaBT(ActionEvent event) {
    }

    public void HomeBT(ActionEvent event) throws IOException {
        //Cambiar de escena al home
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Home.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Home");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void artisBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de artista
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarArtista.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar artista");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void generoBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de genero
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarGenero.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tarjetaBT(ActionEvent event) {
    }

    public void cancionBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de canciones
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCancion.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar canciones");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void composBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de compositores
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCompositor.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar compositor");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void cerrarSesionBT(ActionEvent event) throws IOException {
        //limpia el id guadado en el dataUsuaios para guarda otro
        gu.cerrarSesion();
        //Cambiar de escena al inicio de sesion
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Inicio sesion");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void aceptarCambio_BT(ActionEvent event) throws Exception {
        try {
            //verifica espacios vacios
            if (anterContra.getText().equals("") || nuevaContr.getText().equals("")) {
                //campos vacios
                campos.setText("Llene todos los campos");
                campos.setVisible(true);
            } else {
                campos.setVisible(false);
                //verificca contraseña actual y la que ingreso
                System.out.println(anterContra.getText());
                System.out.println(myUser.get(0).getContrasenna());
                if (anterContra.getText().equals(myUser.get(0).getContrasenna().replace("   ",""))) {
                    campos.setVisible(false);
                    //enviar cambio
                    gu.modificarContra(nuevaContr.getText(), myUser.get(0).getCorreoElectronico());
                    anterContra.setText("");
                    nuevaContr.setText("");
                } else {
                    //contraseña incorrecta
                    campos.setText("Contraseña incorrecta");
                    campos.setVisible(true);
                }
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }
}
