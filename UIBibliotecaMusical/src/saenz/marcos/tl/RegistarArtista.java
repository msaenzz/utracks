package saenz.marcos.tl;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.Genero.Genero;
import saenz.marcos.bl.entities.pais.Pais;
import saenz.marcos.bl.logica.GestorArtista;
import saenz.marcos.bl.logica.GestorGenero;
import saenz.marcos.bl.logica.GestorPais;
import saenz.marcos.ui.Main;
import saenz.marcos.utils.LogManager;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.*;

public class RegistarArtista implements Initializable {

    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXTextField txtApellido;
    @FXML
    private JFXTextField txtUsername;
    @FXML
    private JFXComboBox BoxPais;
    @FXML
    private JFXComboBox BoxGenero;
    @FXML
    private JFXDatePicker fechaNaci;
    @FXML
    private JFXDatePicker fechaDefun;
    @FXML
    private JFXTextField txtEdad;
    @FXML
    private JFXTextArea txtDescr;
    @FXML
    private Text campos;

    public static GestorPais gp = new GestorPais();
    public static ArrayList<Pais> pais = new ArrayList<>();
    public static GestorGenero gg = new GestorGenero();
    public static ArrayList<Genero> genero = new ArrayList<>();
    public static GestorArtista ga = new GestorArtista();
    public static LogManager log = new LogManager();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            autoSelect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Validar campos
    public boolean validarCamposVacios() {
        return !(txtApellido.getText().equals("") || txtUsername.getText().equals("") || txtName.getText().equals("") ||
                fechaNaci.getValue().equals("") || fechaDefun.getValue().equals("") || txtDescr.getText().equals("")
                || txtEdad.getText().equals("") || BoxPais.getValue().equals("                           ")
                || BoxPais.getValue().equals("                           "));
    }

    //select que llena los campos
    private void autoSelect() throws Exception {
        pais = gp.listarPaises();
        genero = gg.listarGeneros();

        for (Pais obj : pais) {
            BoxPais.getItems().addAll(obj.getNombre());
            BoxPais.setPromptText(obj.getNombre());
            BoxPais.setValue("                           ");
        }

        for (Genero obj : genero) {
            BoxGenero.getItems().addAll(obj.getNombre());
            BoxGenero.setPromptText(obj.getNombre());
            BoxGenero.setValue("                           ");
        }
    }

    //extraer id genero
    public String idGenero() {
        for (Genero obj : genero) {
            if (BoxGenero.getValue().equals(obj.getNombre()))
                return obj.getId();
        }
        return "";
    }

    public void listarArtistasBT(ActionEvent event) throws IOException {
        //Cambiar de escena
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoArtista.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Listado artista");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void registroBT(ActionEvent event) throws Exception {
        try {
            if (validarCamposVacios()) {
                campos.setVisible(false);
                Genero migen = new Genero();
                migen.setId(idGenero());
                System.out.println(migen.getId());
                ga.registrarArtista(txtName.getText(), txtApellido.getText(), txtUsername.getText(), fechaNaci.getValue(),
                        fechaDefun.getValue(), migen, Integer.parseInt(txtEdad.getText()), txtDescr.getText(), BoxPais.getValue().toString());
                //Cambiar de escena
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoArtista.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Listado artista");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            } else {
                campos.setVisible(true);
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }
}
