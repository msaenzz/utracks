package saenz.marcos.tl;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.Album.Album;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.bl.entities.Compositor.Compositor;
import saenz.marcos.bl.entities.Genero.Genero;
import saenz.marcos.bl.logica.*;
import saenz.marcos.ui.Main;
import saenz.marcos.utils.LogManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.*;

public class RegistrarCancion implements Initializable {

    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXComboBox BoxGenero;
    @FXML
    private JFXComboBox BoxArtista;
    @FXML
    private JFXComboBox BoxCompositor;
    @FXML
    private JFXComboBox BoxAlbum;
    @FXML
    private JFXDatePicker fechaNaci;
    @FXML
    private JFXButton urlBT;
    @FXML
    private Text campos;

    public static GestorGenero gg = new GestorGenero();
    public static ArrayList<Genero> genero = new ArrayList<>();
    public static GestorArtista ga = new GestorArtista();
    public static ArrayList<Artista> artista = new ArrayList<>();
    public static GestorCompositor gc = new GestorCompositor();
    public static ArrayList<Compositor> compositor = new ArrayList<>();
    public static GestorAlbum gal = new GestorAlbum();
    public static ArrayList<Album> albums = new ArrayList<>();
    public static GestorCancion gcan = new GestorCancion();
    public static LogManager log = new LogManager();
    private String url = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarURL();
        try {
            autoSelect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Validar campos
    public boolean validarCamposVacios() {
        return !(BoxCompositor.getValue().equals("                           ") || txtName.getText().equals("") ||
                fechaNaci.getValue().equals("") || BoxGenero.getValue().equals("                           ")
                || BoxArtista.getValue().equals("                           ") || url == null);
    }

    //select que llena los campos
    private void autoSelect() throws Exception {
        compositor = gc.listarCompositor();
        genero = gg.listarGeneros();
        artista = ga.listarArtistas();

        for (Compositor obj : compositor) {
            BoxCompositor.getItems().addAll(obj.getNombre());
            BoxCompositor.setPromptText(obj.getNombre());
            BoxCompositor.setValue("                           ");
        }

        for (Genero obj : genero) {
            BoxGenero.getItems().addAll(obj.getNombre());
            BoxGenero.setPromptText(obj.getNombre());
            BoxGenero.setValue("                           ");
        }

        for (Artista obj : artista) {
            BoxArtista.getItems().addAll(obj.getNombreArtistico());
            BoxArtista.setPromptText(obj.getNombreArtistico());
            BoxArtista.setValue("                           ");
        }
    }

    //extraer id genero
    public String idGenero(){
        for(Genero obj: genero){
            if(BoxGenero.getValue().equals(obj.getNombre()))
                return obj.getId();
        }
        return "";
    }

    //extraer id artsita
    public String idArtista(){
        for(Artista obj: artista){
            if(BoxArtista.getValue().equals(obj.getNombreArtistico()))
                return obj.getId();
        }
        return "";
    }

    //extraer id compositor
    public String idCompositor(){
        for(Compositor obj: compositor){
            if(BoxCompositor.getValue().equals(obj.getNombre()))
                return obj.getId();
        }
        return "";
    }

    //extraer id compositor
    public String idAlbum(){
        for(Album obj: albums){
            if(BoxAlbum.getValue().equals(obj.getNombre()))
                return obj.getId();
        }
        return "";
    }

    //cargar url cancion
    public void cargarURL() {
        //setea una accion cuando se clickea el botón con el id 'URLButton'
        urlBT.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Cancion");
            //Se especifica el directorio donde va a abrir para escoger la cancion
            fileChooser.setInitialDirectory(new File("src/saenz/marcos/ui/songs"));
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Audio", "*.*"),
                    new FileChooser.ExtensionFilter("ogg", "*.mp3"),
                    new FileChooser.ExtensionFilter("mp4", "*.png")
            );

            try {
                // Obtener la cancion seleccionada
                File songFile = fileChooser.showOpenDialog(stage);
                url = songFile.getName();
                System.out.println("song: " + url);

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
    }

    public void registroBT(ActionEvent event) {
        try {
            if(validarCamposVacios()){
                campos.setVisible(false);
                //extraer id genero
                Genero migen = new Genero();
                migen.setId(idGenero());
                System.out.println(migen.getId());
                //extraer id artista
                Artista miart = new Artista();
                miart.setId(idArtista());
                System.out.println(miart.getId());
                //extraer id compositor
                Compositor micom = new Compositor();
                micom.setId(idCompositor());
                System.out.println(micom.getId());
                //extraer id album
                Album mialb = new Album();
                if(!BoxAlbum.getPromptText().equals("                           ")){
                    mialb.setId(idAlbum());
                    System.out.println(mialb.getId());
                }else{
                    mialb.setId("11111");
                }
                gcan.registrarCancion(url,txtName.getText(),migen,miart,micom,mialb,fechaNaci.getValue());
                //Cambiar de escena
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoCancion.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Listado cancion");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            }else{
                campos.setVisible(true);;
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }

    public void listarCancionBT(ActionEvent event) throws IOException {
        //Cambiar de escena
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoCancion.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Listado cancion");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void listarAlbumBT(ActionEvent event) {
    }
}
