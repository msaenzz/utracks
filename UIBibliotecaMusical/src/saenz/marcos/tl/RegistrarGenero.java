package saenz.marcos.tl;

import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import saenz.marcos.bl.logica.GestorGenero;
import saenz.marcos.ui.Main;
import saenz.marcos.utils.LogManager;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.*;

public class RegistrarGenero implements Initializable {

    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXTextArea txtDescr;
    @FXML
    private Text campos;

    GestorGenero gg = new GestorGenero();
    public static LogManager log = new LogManager();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }


    //Validar campos
    public boolean validarCamposVacios() {
        return !(txtName.getText().equals("") || txtDescr.getText().equals(""));
    }

    public void registroBT(ActionEvent event) throws Exception {
        try {
            if (validarCamposVacios()) {
                campos.setVisible(false);
                gg.registrarGenero(txtName.getText(), txtDescr.getText());
                //Cambiar de escena al Lisatdo genero
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoGenero.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Registrar genero");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            } else {
                campos.setVisible(true);
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }

    public void listarGeneroBT(ActionEvent event) throws IOException {
        //Cambiar de escena al Lisatdo genero
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoGenero.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Listado genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

}
