package saenz.marcos.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.Usuario.Usuario;
import saenz.marcos.bl.logica.GestorUsuario;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Home implements Initializable {

    public GestorUsuario gu = new GestorUsuario();
    public ArrayList<Usuario> us = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void perfilBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de perfil
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Perfil.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Perfil");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tiendaBT(ActionEvent event) {
    }

    public void artisBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de artista
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarArtista.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void generoBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de genero
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarGenero.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tarjetaBT(ActionEvent event) {
    }

    public void cancionBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de canciones
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCancion.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar canciones");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void composBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de compositores
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCompositor.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void cerrarSesionBT(ActionEvent event) throws IOException {
        //limpia el id guadado en el dataUsuaios para guarda otro
        gu.cerrarSesion();
        //Cambiar de escena al inicio de sesion
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Inicio sesion");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }


    // barra horizontal
    public void albumBT(ActionEvent event) throws IOException {
        //Cambiar de escena al listado album
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarAlbum.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Album");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void bibliotecaBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de perfil
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/BibliotecaMusical.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Biblioteca");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void colaBT(ActionEvent event) {
    }

    public void playlistBT(ActionEvent event) {
    }

    public void HomeBT(ActionEvent event) throws IOException {
        //Cambiar de escena al home
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Home.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Home");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }
}
