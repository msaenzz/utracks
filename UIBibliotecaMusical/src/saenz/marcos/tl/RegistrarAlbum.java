package saenz.marcos.tl;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.Artista.Artista;
import saenz.marcos.bl.logica.GestorAlbum;
import saenz.marcos.bl.logica.GestorArtista;
import saenz.marcos.utils.LogManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class RegistrarAlbum implements Initializable {

    @FXML
    private JFXComboBox BoxArtista;
    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXDatePicker fechaNaci;
    @FXML
    private ImageView image;
    @FXML
    private JFXButton addImageBT;
    @FXML
    private Text campos;

    public static LogManager log = new LogManager();
    public static GestorArtista ga = new GestorArtista();
    public static GestorAlbum gart = new GestorAlbum();
    public static ArrayList<Artista> artista = new ArrayList<>();
    private String url = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            autoSelect();
            cargarImagen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Validar campos
    public boolean validarCamposVacios() {
        return !(txtName.getText().equals("") || url == null
                || BoxArtista.getValue().equals("                           "));
    }

    private void autoSelect() throws Exception {
        artista = ga.listarArtistas();

        for (Artista obj : artista) {
            BoxArtista.getItems().addAll(obj.getNombreArtistico());
            BoxArtista.setPromptText(obj.getNombreArtistico());
            BoxArtista.setValue("                           ");
        }
    }

    //extraer id artsita
    public String idArtista() {
        for (Artista obj : artista) {
            if (BoxArtista.getValue().equals(obj.getNombreArtistico()))
                return obj.getId();
        }
        return "";
    }

    //para cargar la imagen y mostrarla
    public void cargarImagen() {
        //setea una accion cuando se clickea el botón con el id 'buttonAddImage'
        addImageBT.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Imagen");
            //Se especifica el directorio donde va a abrir para escoger la imagen
            fileChooser.setInitialDirectory(new File("src/saenz/marcos/ui/img/album"));
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );

            // Obtener la imagen seleccionada
            File imgFile = fileChooser.showOpenDialog(stage);

            // Mostrar la imagen
            if (imgFile != null) {
                url = imgFile.getName();
                System.out.println("imagen: " + url);
                Image img = new Image("file:" + imgFile.getAbsolutePath());
                image.setImage(img);
            }
        });
    }

    public void registroBT(ActionEvent event) throws Exception{
        try {
            if (validarCamposVacios()) {
                //extraer id artista
                Artista miart = new Artista();
                miart.setId(idArtista());
                gart.registrarAlbum(txtName.getText(), fechaNaci.getValue(), miart, url);
                //Cambiar de escena al listado album
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoAlbum.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Album");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            } else {
                campos.setVisible(true);
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        }
    }

    public void listarAlbumBT(ActionEvent event) throws IOException {
        //Cambiar de escena al listado album
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/ListadoAlbum.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Album");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }
}
