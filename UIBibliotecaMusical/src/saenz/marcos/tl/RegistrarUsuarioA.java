package saenz.marcos.tl;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import saenz.marcos.bl.logica.GestorUsuario;
import saenz.marcos.utils.LogManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.*;

public class RegistrarUsuarioA implements Initializable {

    @FXML
    private JFXTextField txtEmail;
    @FXML
    private JFXTextField txtUsername;
    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXTextField txtApellidoUno;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtApellidoDos;
    @FXML
    private ImageView image;
    @FXML
    private JFXButton addImageBT;
    @FXML
    private Text campos;

    //Atributos propios
    private String url = null;
    public static GestorUsuario gu = new GestorUsuario();
    public static LogManager log = new LogManager();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarImagen();
    }

    //para cargar la imagen y mostrarla
    public void cargarImagen() {
        //setea una accion cuando se clickea el botón con el id 'buttonAddImage'
        addImageBT.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Imagen");
            //Se especifica el directorio donde va a abrir para escoger la imagen
            fileChooser.setInitialDirectory(new File("src/saenz/marcos/ui/img/perfil"));
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );

            // Obtener la imagen seleccionada
            File imgFile = fileChooser.showOpenDialog(stage);

            // Mostrar la imagen
            if (imgFile != null) {
                url = imgFile.getName();
                System.out.println("imagen: " + url);
                Image img = new Image("file:" + imgFile.getAbsolutePath());
                image.setImage(img);
            }
        });
    }

    //Validar campos
    public boolean validarCamposVacios() {
        return !(txtEmail.getText().equals("") || txtUsername.getText().equals("") || txtName.getText().equals("") ||
                txtApellidoUno.getText().equals("") || txtId.getText().equals("") || txtApellidoDos.getText().equals("")
                || url == null);
    }

    public void registroBT(ActionEvent event) throws Exception {
        try {
            if(validarCamposVacios()){
                campos.setVisible(false);
                gu.registrarUsuario(txtId.getText(),txtName.getText(),txtApellidoUno.getText(),txtApellidoDos.getText(),
                        txtEmail.getText(),txtUsername.getText(),url,"Administrador");
                gu.registrarAdministrador(txtId.getText());
                //Cambiar de escena
                Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
                Scene changeScene = new Scene(changeToLis);

                thisWindow.setTitle("Inicio sesion");
                thisWindow.setScene(changeScene);
                thisWindow.show();
            }else{
                campos.setVisible(true);;
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }

    public void iniciarSesion(ActionEvent event) throws IOException {
        //Cambiar de escena
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Inicio sesion");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }
}
