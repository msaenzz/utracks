package saenz.marcos.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.Compositor.Compositor;
import saenz.marcos.bl.logica.GestorCompositor;
import saenz.marcos.bl.logica.GestorUsuario;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ListadoCompositor implements Initializable {

    @FXML
    private TableView TBLFather;
    @FXML
    private TableColumn col_1;
    @FXML
    private TableColumn col_2;
    @FXML
    private TableColumn col_3;
    @FXML
    private TextField buscar;

    public static GestorCompositor gc = new GestorCompositor();
    public GestorUsuario gu = new GestorUsuario();
    ArrayList<Compositor> compositors = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            compositors = gc.listarCompositor();
            mostrarInfor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mostrarInfor() {
        //limpia la tabla antes de listar por si habian datos agregados anteriorimente
        TBLFather.getItems().clear();
        //debe poner el nombre de las variables exactamente como viene en el objeto
        col_1.setCellValueFactory(new PropertyValueFactory("nombre"));
        col_2.setCellValueFactory(new PropertyValueFactory("apellido"));
        col_3.setCellValueFactory(new PropertyValueFactory("paisNacimiento"));

        for (int i = 0; i < compositors.size(); i++) {
            TBLFather.getItems().add(compositors.get(i));
        }
    }

    //sobrecarga de metodos
    public void mostrarInfor(int i) {
        TBLFather.getItems().add(compositors.get(i));
    }

    //evento cuando que se activa cuando se escribe en el textfield de buscar
    public void buscarCompositor(KeyEvent keyEvent) {
        ArrayList<Integer> datos = new ArrayList();
        for (int i = 0; i < compositors.size(); i++) {
            //se encarga de buscar en el textfield y en el arreglo, datos que coincidan para agregar en el arreglo datos las posiciones
            if (compositors.get(i).getNombre().toLowerCase().contains(buscar.getText().toLowerCase()) ||
                    compositors.get(i).getApellido().toLowerCase().contains(buscar.getText().toLowerCase()) ||
                    compositors.get(i).getPaisNacimiento().toLowerCase().contains(buscar.getText().toLowerCase())) datos.add(i);
        }
        //limpia la tabla y se encarga de insertar los datos en las posisiones que encontro coincidencias
        TBLFather.getItems().clear();
        for (int i = 0; i < datos.size(); i++) {
            mostrarInfor(datos.get(i)); //datos [0] = 0, datos[1] = 2
        }
    }


    public void perfilBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de perfil
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Perfil.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Perfil");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tiendaBT(ActionEvent event) {
    }

    public void HomeBT(ActionEvent event) throws IOException {
        //Cambiar de escena al home
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Home.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Home");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void artisBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de artista
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarArtista.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar artista");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void generoBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de genero
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarGenero.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tarjetaBT(ActionEvent event) {
    }

    public void cancionBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de canciones
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCancion.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar canciones");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void composBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de compositores
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCompositor.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar compositor");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void cerrarSesionBT(ActionEvent event) throws IOException {
        //limpia el id guadado en el dataUsuaios para guarda otro
        gu.cerrarSesion();
        //Cambiar de escena al inicio de sesion
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Inicio sesion");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }
}
