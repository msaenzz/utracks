package saenz.marcos.tl;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import saenz.marcos.bl.logica.GestorUsuario;
import saenz.marcos.utils.LogManager;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.*;

public class RecuperarContrasena implements Initializable {

    @FXML
    private JFXTextField txtEmail;
    @FXML
    private Text campos;

    public static GestorUsuario gu = new GestorUsuario();
    public static LogManager log = new LogManager();

    @Override


    public void initialize(URL location, ResourceBundle resources) {

    }

    public void ingresar(ActionEvent event) throws Exception {
        try {
            if (txtEmail.getText().equals("")) {
                campos.setVisible(true);
                campos.setText("Ingrese el correo");
            } else {
                //enviar correo
                if (gu.recuperarContrasenna(txtEmail.getText())) {
                    campos.setVisible(false);
                    //Cambiar de escena
                    Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
                    Scene changeScene = new Scene(changeToLis);

                    thisWindow.setTitle("Inicio sesion");
                    thisWindow.setScene(changeScene);
                    thisWindow.show();
                } else {
                    campos.setText("Correo no encontrado");
                    campos.setVisible(true);
                }
            }
        } catch (NullPointerException e) {
            log.LOG_LOCAL.warning("Error del sistema");
            log.LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            log.LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }
    }

    public void volver(ActionEvent event) throws IOException {
        //Cambiar de escena
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Inicio sesion");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }
}
