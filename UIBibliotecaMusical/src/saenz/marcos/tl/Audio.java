package saenz.marcos.tl;

import javafx.fxml.FXML;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class Audio {

    @FXML
    private static MediaPlayer mp;
    @FXML private static Media me;


    public void inicio2(String url, int cont){

        inicio(url, cont);
        System.out.println(url);
    }

    public void inicio(String url, int cont)  {
        System.out.println(url);
        try {
            if (cont > 0) {
                mp.pause();
                pause();
                String path = new File("src/saenz/marcos/ui/songs/" + url).getAbsolutePath();
                me = new Media(new File(path).toURI().toString());
                mp = new MediaPlayer(me);
                play();
                cont++;
            } else {
                String path = new File("src/saenz/marcos/ui/songs/" + url).getAbsolutePath();
                me = new Media(new File(path).toURI().toString());
                mp = new MediaPlayer(me);
                play();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void play() {
        mp.play();
    }

    public void pause() {
        mp.pause();
    }

    public void fast(javafx.event.ActionEvent actionEvent) {
        mp.setRate(2);
    }

    public void slow(javafx.event.ActionEvent actionEvent) {
        mp.setRate(.5);
    }

    public void reload() {
        mp.seek(mp.getStartTime());
        mp.play();
    }

    public void start() {
        mp.seek(mp.getStartTime());
        mp.stop();
    }

    public void last() {
        mp.seek(mp.getTotalDuration());
        mp.stop();
    }
}
