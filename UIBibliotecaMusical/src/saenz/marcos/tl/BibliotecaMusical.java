package saenz.marcos.tl;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import saenz.marcos.bl.entities.Cancion.Cancion;
import saenz.marcos.bl.logica.GestorCancion;
import saenz.marcos.bl.logica.GestorUsuario;
import saenz.marcos.utils.LogManager;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class BibliotecaMusical implements Initializable {

    @FXML
    private TableView TBLFather;
    @FXML
    private TableColumn col_1;
    @FXML
    private TextField buscar;
    @FXML
    private Text txtNombreArtista;
    @FXML
    private Text txtNombreCancion;
    @FXML
    private JFXButton nextIdBT;
    @FXML
    private JFXButton backIdBT;
    @FXML
    private JFXButton pauseIdBT;
    @FXML
    private JFXButton playIdBT;

    public GestorUsuario gu = new GestorUsuario();
    public GestorCancion gc = new GestorCancion();
    ArrayList<Cancion> cancion = new ArrayList<>();
    public static String url = "";
    public static int cont = 0;
    public static Audio audio = new Audio();
    public static LogManager log = new LogManager();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            cancion = gc.listarCancionesBiblio();
            mostrarInfor();
            clickCancion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mostrarInfor() {
        //limpia la tabla antes de listar por si habian datos agregados anteriorimente
        TBLFather.getItems().clear();
        //debe poner el nombre de las variables exactamente como viene en el objeto
        col_1.setCellValueFactory(new PropertyValueFactory("nombre"));

        for (int i = 0; i < cancion.size(); i++) {
            TBLFather.getItems().add(cancion.get(i));
        }
    }

    //sobrecarga de metodos
    public void mostrarInfor(int i) {
        TBLFather.getItems().add(cancion.get(i));
    }

    //evento cuando que se activa cuando se escribe en el textfield de buscar
    public void buscarCompositor(KeyEvent keyEvent) {
        ArrayList<Integer> datos = new ArrayList();
        for (int i = 0; i < cancion.size(); i++) {
            //se encarga de buscar en el textfield y en el arreglo, datos que coincidan para agregar en el arreglo datos las posiciones
            if (cancion.get(i).getNombre().toLowerCase().contains(buscar.getText().toLowerCase())) datos.add(i);
        }
        //limpia la tabla y se encarga de insertar los datos en las posisiones que encontro coincidencias
        TBLFather.getItems().clear();
        for (int i = 0; i < datos.size(); i++) {
            mostrarInfor(datos.get(i)); //datos [0] = 0, datos[1] = 2
        }
    }

    //cuando se clickea una cancion del listado
    public void clickCancion() {
        TBLFather.setOnMousePressed(event -> {
            //exctaer el url de la tabla padre
            url = TBLFather.getSelectionModel().getSelectedItem().toString();
            System.out.println("Listado: " + url); // url de la cancion

            //for que llena el nombre y el artista de la cancion que se clickeo
            for(Cancion obj: cancion){
                if(obj.getUrl().equals(url)){
                    txtNombreArtista.setText(obj.getArtista().getNombreArtistico());
                    txtNombreCancion.setText(obj.getNombre());
                }
            }
            reproducir();
        });
    }

    // reproductor inicial
    public void reproducir(){
        //lo manda a la clase audio para reproducir la cancion
        audio.inicio(url,cont);
        cont++;
        //pause ||
        playIdBT.setVisible(false);
        playIdBT.setDisable(false);
        pauseIdBT.setVisible(true);
    }

    public void playBT(ActionEvent event) {
        //pause ||
        playIdBT.setVisible(false);
        playIdBT.setDisable(true);
        pauseIdBT.setDisable(false);
        pauseIdBT.setVisible(true);
        audio.play();
    }

    public void nextBT(ActionEvent event) {
        for (int i = 0; i < cancion.size(); i++) {
            if(url.equals(cancion.get(i).getUrl())){
                if(i==2){
                    try {
                        i--;
                        url = cancion.get(i).getUrl();
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                }else {
                    i++;
                    url = cancion.get(i).getUrl();
                    txtNombreArtista.setText(cancion.get(i).getArtista().getNombreArtistico());
                    txtNombreCancion.setText(cancion.get(i).getNombre());
                    reproducir();
                }
            }
        }
    }

    public void backBT(ActionEvent event) {
        for (int i = 0; i < cancion.size(); i++) {
            if(url.equals(cancion.get(i).getUrl())){
                if(i==0){
                    try {
                        url = cancion.get(i++).getUrl();
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                }else {
                    url = cancion.get(i - 1).getUrl();
                    txtNombreArtista.setText(cancion.get(i - 1).getArtista().getNombreArtistico());
                    txtNombreCancion.setText(cancion.get(i - 1).getNombre());
                    reproducir();
                }
            }
        }
    }

    public void pauseBT(ActionEvent event) {
        //play >
        pauseIdBT.setVisible(false);
        pauseIdBT.setDisable(true);
        playIdBT.setVisible(true);
        playIdBT.setDisable(false);
        audio.pause();

    }

    public void perfilBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de perfil
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Perfil.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Perfil");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tiendaBT(ActionEvent event) {
    }

    public void artisBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de artista
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarArtista.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void generoBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de genero
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarGenero.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void tarjetaBT(ActionEvent event) {
    }

    public void cancionBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de canciones
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCancion.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar canciones");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void composBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de compositores
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/RegistrarCompositor.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Registrar genero");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void cerrarSesionBT(ActionEvent event) throws IOException {
        //limpia el id guadado en el dataUsuaios para guarda otro
        gu.cerrarSesion();
        //Cambiar de escena al inicio de sesion
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Login.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Inicio sesion");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }


    // barra horizontal
    public void albumBT(ActionEvent event) {
    }

    public void bibliotecaBT(ActionEvent event) throws IOException {
        //Cambiar de escena al registro de perfil
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/BibliotecaMusical.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Biblioteca");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }

    public void colaBT(ActionEvent event) {
    }

    public void playlistBT(ActionEvent event) {
    }

    public void HomeBT(ActionEvent event) throws IOException {
        //Cambiar de escena al home
        Stage thisWindow = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent changeToLis = FXMLLoader.load(getClass().getResource("/saenz/marcos/ui/fxml/Home.fxml"));
        Scene changeScene = new Scene(changeToLis);

        thisWindow.setTitle("Home");
        thisWindow.setScene(changeScene);
        thisWindow.show();
    }
}
